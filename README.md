# Edición de videos con python

Clase pública de programación básica, se muestra la herramienta jupyter
notebooks, la biblioteca de visualización bokeh y moviepy, para edición de
videos.

En el archivo ``macrisis.ipynb`` se encuentra el contenido.

Esta clase fue dada en el marco de la protesta de los docentes universitarios
por los recortes que está padeciendo la Universidad Pública.

Se transita la tercer semana del no dictado ni inicio de clases y la jornada
del día 22-08-2018 se protesta bajo la consigna:

**PARO DOCENTE-ESTUDIANTIL**

**DEFENDAMOS LA UNIVERSIDAD PÚBLICA**

**SIN EDUCACIÓN NO HAY FUTURO**

